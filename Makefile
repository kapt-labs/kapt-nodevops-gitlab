# Provisioners management
# --

provisioning_path = ./provisioning/
compose_file = $(provisioning_path)docker-compose.yml
tmp_folder_path = $(provisioning_path)_provisioners/devenv/.local_home_settings

stop_all: umount
	@printf "[ ... ] Stopping all containers ..."
	@docker stop $$(docker ps -a -q) > /dev/null 2>&1 || true
	@printf "\r[ \e[32;1mOK!\e[0m ]\n"

rm_all: stop_all
	@printf "[ ... ] Deleting all containers ..."
	@docker rm $$(docker ps -a -q) > /dev/null 2>&1 || true
	@printf "\r[ \e[32;1mOK!\e[0m ]\n"

rm_all_images: rm_all
	@printf "[ ... ] Deleting all images ..."
	@docker rmi $$(docker images -q) -f > /dev/null 2>&1 || true
	@printf "\r[ \e[32;1mOK!\e[0m ]\n"

env: check_env_KAPT_NODEVOPS_MOUNT_VIRTUALENV check_env_KAPT_NODEVOPS_MOUNT_WORKSPACE check_env_KAPT_NODEVOPS_HOST_WORKSPACE check_env_KAPT_NODEVOPS_HOST_VIRTUALENV copy_files
	@printf "\r[ \e[32;1mOK!\e[0m ]\n"
	@./menu.sh run $(compose_file)

build: copy_files
	@./menu.sh build $(compose_file)

mount:
	@./menu.sh mount

umount:
	@./menu.sh umount

check_env_%:
	@printf "[ ... ] Checking if $* is set ..."
	@ if [ "${${*}}" = "" ]; then \
        printf "\r[ \e[31;1mKO!\e[0m ]\n"; \
        exit 1; \
	else \
		printf "\r[ \e[32;1mOK!\e[0m ]\n"; \
    fi

copy_files:
	@printf "[ ... ] Copying files .."
	@rm -Rf $(tmp_folder_path)
	@mkdir -p $(tmp_folder_path)
	@cp -R ~/.pip $(tmp_folder_path)/
	@cp -R ~/.pypirc $(tmp_folder_path)/
	@cp -R ~/.gitconfig $(tmp_folder_path)/
	@cp -R ~/.hgrc $(tmp_folder_path)/
	@cp -R ~/.ssh $(tmp_folder_path)/
	@cp -R ~/.zshrc $(tmp_folder_path)/ 2>/dev/null || :
	@cp -R ~/.oh-my-zsh $(tmp_folder_path)/ 2>/dev/null || :

