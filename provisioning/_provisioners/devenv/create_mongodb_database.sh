#!/bin/bash

# Admin User
MONGODB_ADMIN_USER=${MONGODB_ADMIN_USER:-"kapt"}
MONGODB_ADMIN_PASS=${MONGODB_ADMIN_PASS:-"kapt"}


# Application Database User
MONGODB_APPLICATION_DATABASE=$1


sleep 3

# If we've defined the MONGODB_APPLICATION_DATABASE environment variable and it's a different database
# than admin, then create the user for that database.
# First it authenticates to Mongo using the admin user it created above.
# Then it switches to the REST API database and runs the createUser command
# to actually create the user and assign it to the database.

echo "=> Creating a ${MONGODB_APPLICATION_DATABASE} database user with a password in MongoDB"
mongo admin -u $MONGODB_ADMIN_USER -p $MONGODB_ADMIN_PASS --host mongodb << EOF
echo "Using $MONGODB_APPLICATION_DATABASE database"
use $MONGODB_APPLICATION_DATABASE
db.createUser({user: '$MONGODB_ADMIN_USER', pwd: '$MONGODB_ADMIN_PASS', roles:[{role:'dbOwner', db:'$MONGODB_APPLICATION_DATABASE'}]})
EOF


sleep 1

echo "MongoDB configured successfully. You may now connect to the DB."
