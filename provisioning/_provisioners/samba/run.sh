#!/bin/sh
chown -R kapt:$(id -g kapt) /workspace/ > /home/kapt/w.log
chown -R kapt:$(id -g kapt) /virtualenvs/ > /home/kapt/v.log
smbd --foreground --log-stdout