#!/bin/bash
dockerparams="mongodb3 postgresql rabbitmq redis phppgadmin portainer app" # mandatory container(s)

# Requirements and argument cheking
printf "[ ... ] Checking system requirements"
if hash dialog 2>/dev/null; then
    printf "\r[ \e[32;1mOK!\e[0m ]\n"
else
    printf "\r[ \e[31;1mKO!\e[0m ]\n"
    echo "This script requires 'dialog'. install it with your favorite package manager and start again."
    exit 1
fi

printf "[ ... ] Checking execution mode ..."
if [ "$1" = "run" ]; then
    printf "\r[ \e[32;1mRUN\e[0m ]\n"
    exec_mode=$1
elif [ "$1" = "build" ]; then
    printf "\r[\e[33;1mBUILD\e[0m]\n"
    exec_mode=$1
elif [ "$1" = "mount" ]; then
    printf "\r[\e[34;1mMOUNT\e[0m]\n"
    printf "[ ... ] Mounting folders ..."
    if [ $(uname -s) = "Linux" ]; then
        sudo mount -o port=2049,vers=4 -v localhost:/ $KAPT_NODEVOPS_MOUNT_WORKSPACE
        sudo mount -o port=2050,vers=4 -v localhost:/ $KAPT_NODEVOPS_MOUNT_VIRTUALENV
    elif [ $(uname -s) = "Darwin" ]; then
        mount_smbfs //kapt:kapt@127.0.0.2/virtualenvs $KAPT_NODEVOPS_MOUNT_VIRTUALENV
	    mount_smbfs //kapt:kapt@127.0.0.2/workspace $KAPT_NODEVOPS_MOUNT_WORKSPACE
    fi
    if [ $? = 0 ]; then
        printf "\r[ \e[32;1mOK!\e[0m ]\n"
    else
        printf "\r[ \e[31;1mKO!\e[0m ]\n"
        exit 1
    fi
    exit
elif [ "$1" = "umount" ]; then
    printf "\r[\e[35;1mUMOUN\e[0m]\n"
    printf "[ ... ] Unmounting folders ..."
    if mount | grep $(echo "$KAPT_NODEVOPS_MOUNT_VIRTUALENV" | rev | cut -c 2- | rev ) 2>&1 > /dev/null || mount | grep $KAPT_NODEVOPS_MOUNT_VIRTUALENV; then
        sudo umount $KAPT_NODEVOPS_MOUNT_VIRTUALENV
    fi
    if mount | grep $(echo "$KAPT_NODEVOPS_MOUNT_WORKSPACE" | rev | cut -c 2- | rev ) 2>&1 > /dev/null || mount | grep $KAPT_NODEVOPS_MOUNT_WORKSPACE; then
	    sudo umount $KAPT_NODEVOPS_MOUNT_WORKSPACE
    fi
    if [ $? = 0 ]; then
        printf "\r[ \e[32;1mOK!\e[0m ]\n"
    else
        printf "\r[ \e[31;1mKO!\e[0m ]\n"
        exit 1
    fi
    exit
else
    printf "\r[ \e[31;1mKO!\e[0m ]\n"
    echo "Usage : $0 [run|build|mount] [path_to_composefile]"
    exit 1
fi

printf "[ ... ] Checking Docker-compose file..."
if [ -f "$2" ]; then
    printf "\r[ \e[32;1mOK!\e[0m ]\n"
    composefile=$2

    if [ $(uname -s) = "Linux" ]; then
        dockerparams=$dockerparams" nfs-server-alpine-workspace nfs-server-alpine-virtualenvs"
        os_compose_file=${composefile/docker-compose.yml/docker-compose.linux.yml}
    elif [ $(uname -s) = "Darwin" ]; then
        dockerparams=$dockerparams" samba"
        os_compose_file=${composefile/docker-compose.yml/docker-compose.macos.yml}
    fi
else
    printf "\r[ \e[31;1mKO!\e[0m ]\n"
    echo "Usage : $0 [run|build|mount] [path_to_composefile]"
    exit 1
fi

# Container selection menu
cmd=(dialog --backtitle "Kapt NoDevOps Setup" --separate-output --checklist "Select containers:" 22 76 16)
options=(0 "Portainer" on
         1 "Elastic Search" off
         2 "MongoDB" on
         21 "-> Mongo-Express (Webmin for MongoDB)" off
         3 "PostgreSQL" on
         31 "-> PHPPgAdmin (Webmin for PgSQL)" on
         4 "MariaDB (MySQL)" off
         41 "-> PHPMyAdmin (Webmin for MySQl)" off
         5 "Redis" on
         51 "-> Redis Commander (Webmin for Redis)" off
         6 "RabbitMQ" on)
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
if [ $? != 0 ]; then
    clear
    printf "[\e[33;1mWARNI\e[0m] User cancelled choice. Exiting ...\n"
    exit 1
fi
clear
for choice in $choices
do
    case $choice in
        1)
            dockerparams="$dockerparams elasticsearch"
            ;;
        2)
            # dockerparams="$dockerparams mongodb3"
            ;;
        21)
            dockerparams="$dockerparams mongo-express"
            ;;
        3)
            # dockerparams="$dockerparams postgresql"
            ;;
        31)
            # dockerparams="$dockerparams phppgadmin"
            ;;
        4)
            dockerparams="$dockerparams mariadb"
            ;;
        41)
            dockerparams="$dockerparams phpmyadmin"
            ;;
        5)
            # dockerparams="$dockerparams redis"
            ;;
        51)
            dockerparams="$dockerparams redis-commander"
            ;;
        6)
            # dockerparams="$dockerparams rabbitmq"
            ;;
    esac
done

# Mount prompt
function mount_prompt () {
    if [ "$exec_mode" = "run" ]; then
        dialog --backtitle "Kapt NoDevOps Setup" --title "Network volumes" --yesno "Would you like to mount network volumes now?" 10 30 2>&1 # 0 y 1 n
        if [ $? == 0 ]; then
            clear
            ./menu.sh mount
        else
            clear
        fi
    fi
}

# Launch Docker-compose
function launch_docker () {
    if [ "$exec_mode" = "run" ]; then
        docker_mode="up -d"
    else
        docker_mode="build"
    fi
    LOCAL_USER_ID=$(id -u) LOCAL_GROUP_ID=$(id -g) KAPT_NODEVOPS_HOST_WORKSPACE=$KAPT_NODEVOPS_HOST_WORKSPACE KAPT_NODEVOPS_HOST_VIRTUALENV=$KAPT_NODEVOPS_HOST_VIRTUALENV docker-compose -f $composefile -f $os_compose_file $docker_mode $dockerparams 2>&1
}

# Per OS detection
if [ $(uname -s) = "Linux" ]; then
    launch_docker
    mount_prompt
elif [ $(uname -s) = "Darwin" ]; then
    dialog --backtitle "Kapt NoDevOps Setup" --title "Superuser access information" --infobox "Your user's password will be prompted to update network configuration." 5 35
    sudo ifconfig lo0 127.0.0.2 alias up # Adding a second IP to the loopback to allow SMB connection.
    launch_docker
    mount_prompt
fi
