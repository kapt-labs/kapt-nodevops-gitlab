KAPT no-devops
==============

*Develop within kapt environment with no devops skills*

**Required configuration**

- Have a working docker host configuration
- Have `dialog`installed on your system.
- Upgrade `kapt-fabfile` with a version > 2.4.0 on all concerned projects
- Have configured properly: `.pypirc`, `.pip/pip.conf`, SSH keys and `.gitconfig` on your host system
- Edit your local `.bashrc`, `.bash_profile` or `.zshrc` file and add:

```sh
    #No-DevOPS Aliases
    alias devenv='docker exec -it dev-env bash'
    alias makedevenv='make -C $HOME/Applications/kapt-nodevops env'
    alias mountdevenv='make -C $HOME/Applications/kapt-nodevops mount'

    # Path to your remote mount workspace and virtualenvs
    export KAPT_NODEVOPS_MOUNT_WORKSPACE=$HOME/workspace/
    export KAPT_NODEVOPS_MOUNT_VIRTUALENV=$HOME/virtualenvs/

    # Path to your local workspace and virtualenvs
    export KAPT_NODEVOPS_HOST_WORKSPACE=$HOME/.host_workspace/
    export KAPT_NODEVOPS_HOST_VIRTUALENV=$HOME/.host_virtualenvs/
```

- If you wish to use zsh in your container, replace the line `alias devenv='docker exec -it dev-env bash'` with `alias devenv='docker exec -it dev-env zsh'` in your local `.bashrc`, `.bash_profile` or `.zshrc` file

**Containers init**

- Go in the main folder and run `make env` to create/launch containers


**How to develop ?**

- Once containers are created/launched, run `dev-env` to go in the running dev container

**What services are included ?**

- elasticsearch reachable on ports 9200 and 9300. To call it from your host system use `localhost`. To call it from the dev container use `elasticsearch`

- mongodb reachable on port 27017. To call it from your host system use `localhost`. To call it from the dev container use `mongodb`

- postgresql reachable on port 5432. To call it from your host system use `localhost`. To call it from the dev container use `postgresql`

- mariadb reachable on port 3306. To call it from your host system use `localhost`. To call it from the dev container use `mariadb`

- rabbitmq reachable on ports 5672 and 8004. To call it from your host system use `localhost`. To call it from the dev container use `rabbitmq`. Rabbitmq provide a web interface to administrate databases at: http://localhost:8004/

- redis reachable on port 6379. To call it from your host system use `localhost`. To call it from the dev container use `redis`.


**What management tools are included ?**

- Mongo express web interface: Allow to easily administrate mongodb. Reachable at http://localhost:8001/

- Phppgadmin web interface: Allow to easily administrate postgresql. Reachable at http://localhost:8002/

- Redis-commander web interface: Allow to easily administrate redis. Reachable at http://localhost:8003/

- Rabbimq management web interface: Allow to easily administrate rabbitmq. Reachable at http://localhost:8004/

- PHPMyAdmin web interface: Allow to easily administrate mariadb.
Reachable at http://localhost:8005/

- Portainer: Allow to easily administrate docker containers, images and volumes. Reachable at http://localhost:8006/


**Password and users**

Everywhere, user is `kapt` and password is `kapt`

**Create databases**

- Postgresql: use http://localhost:8002/ and create a database for all of your projects

- mariadb: use http://localhost:8005/ and create a database for all of your projects

- Mongodb: connect to the dev container using the dev-env command and then run `create_mongodb_database new_database_name` to create a database called `new_database_name`

- Redis: Just change the db number in your project settings

- Rabbitqm: Create a db using web interface at http://localhost:8004/


**Uses cases**

You can have three different strategies regarding this module:

- Dont use it, in this case you have nothing more to do

- Use only the databases containers.
In this case you need to add an env variable to your local .bashrc/.bash_profile: `export FABRIC_CONF_TYPE='db_nodevops'`. In this case, you need to configure your secrets.json in all of your projects defining the following values:

```

    "DB_NAME": "my_db_name",
    "DB_USER": "kapt",
    "DB_PASSWORD": "kapt",
    "DB_HOST": "localhost",
    "MONGODB_NAME": "my_mongodb_name",
    "MONGODB_HOST": "localhost",
    "MONGODB_USER": "kapt",
    "MONGODB_PASSWORD": "kapt",
    "REDIS_HOST": "localhost",
    "REDIS_DB": a number different in all projects,
```

- Use the dev-env container. In this case you only have to configure your secrets with the following on all projects:

```

    "DB_NAME": "my_db_name",
    "DB_USER": "kapt",
    "DB_PASSWORD": "kapt",
    "DB_HOST": "postgresql",
    "MONGODB_NAME": "my_mongodb_name",
    "MONGODB_HOST": "mongodb",
    "MONGODB_USER": "kapt",
    "MONGODB_PASSWORD": "kapt",
    "REDIS_HOST": "redis",
    "REDIS_DB": a number different in all projects,
```
